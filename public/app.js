function hover(element) {

    if (element.getAttribute('id') === 'PD') {
        element.setAttribute('src', 'images/PD_C.png');
    }
    if (element.getAttribute('id') === 'DV') {
        element.setAttribute('src', 'images/DtV02_C.png');
    }
    if (element.getAttribute('id') === 'WE') {
        element.setAttribute('src', 'images/WzE_C.png');
    }
    if (element.getAttribute('id') === 'DU') {
        element.setAttribute('src', 'images/DGS02_c.png');
    }
if (element.getAttribute('id') === 'KW') {
        element.setAttribute('src', 'images/KW01_C.png');
    }
}

function unhover(element) {
    if (element.getAttribute('id') === 'PD') {
        element.setAttribute('src', 'images/PD.png');
    }
    if (element.getAttribute('id') === 'DV') {
        element.setAttribute('src', 'images/DtV02.png');
    }
    if (element.getAttribute('id') === 'WE') {
        element.setAttribute('src', 'images/WzE.png');
    }
    if (element.getAttribute('id') === 'DU') {
        element.setAttribute('src', 'images/DGS02.PNG');
    }
if (element.getAttribute('id') === 'KW') {
        element.setAttribute('src', 'images/KW01.png');
    }
}

/* TODO Code for implementing star-sky-animation (eventually later) */

/*
const STAR_COORDS = [
    {
        "x": 1596,
        "y": 578
    },
    {
        "x": 609,
        "y": 379
    }
];

const svgElement = document.createElementNS("http://www.w3.org/2000/svg", 'svg');
svgElement.setAttribute('viewbox', '0 0 2000 2000');
svgElement.setAttribute('preserveaspectratio', 'xMinYMin slice');
svgElement.setAttribute('width', '100%');
svgElement.setAttribute('height', '100%');

document.body.appendChild(svgElement);

STAR_COORDS.forEach(({x, y}, index) => {
    //star circle shape
    const star = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
    star.setAttribute('r', 1);
    star.setAttribute('fill', 'white');
    star.classList.add('star');

    // group to hold the translation
    const starTranslate = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    starTranslate.setAttribute('transform', 'translate(${x} ${y})');
    starTranslate.appendChild(star);

    // add to existing svg element
    svgElement.appendChild(starTranslate);


});
*/
