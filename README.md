## About Me


This *About Me* page is meant to give a more visual access to my Coding-Projects
than my GitLab-Profile.
You can take a look at the Page via [opulantus.gitlab.io/about-me](http://opulantus.gitlab.io/about-me/).


##### What did I use to build the page?

The foundation of the styling of the page consists of the CSS-framework [Bulma](https://bulma.io/)
comlemented by my own CSS specifications. The on-scroll-animations are realized
with the [AOS-Framework](https://michalsnik.github.io/aos/). [Coolors](https://coolors.co/5299d3-e5e9ef-ebe3cc-9dd8ff-774667)
helped me to find a color-scheme, and [cssgradient.io](https://cssgradient.io/)
to adjust the background gradient. Last but not least, I used [Fontawesome](https://fontawesome.com/)
for the various Icons on the page.